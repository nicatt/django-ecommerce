from django.urls import path
from api import views

urlpatterns = [
    path('getBrands/', views.get_brand)
]
