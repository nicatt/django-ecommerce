from django.http import Http404


class APIMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.request = request

        # api ilə başlamayan route-a request atılıbsa
        if not request.path.startswith('/api'):
            return self.get_response(request)
        else:
            # request post-dursa və xhr-dirsə qəbul et
            if self.is_method_post() and self.is_xhr_request():
                return self.get_response(request)
            else:
                raise Http404

    def is_method_post(self):
        return self.request.method == 'POST'

    def is_xhr_request(self):
        headers = self.request.headers
        return headers['X-Requested-With'] is not None and headers['X-Requested-With'] == 'XMLHttpRequest'
