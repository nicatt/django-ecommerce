from django.http import JsonResponse, HttpResponse
from django.core import serializers
from main.models import ModelSeries, Model
from main.utils import check_params
import json


def get_brand(request):
    if not check_params(request.POST, ['id']):
        return JsonResponse({
            'status': False,
            'error': 'Invalid params'
        })

    data = []
    series = ModelSeries.objects.filter(brand=request.POST.get('id')).values('id', 'name')

    for s in series:
        s['models'] = []
        models = Model.objects.filter(series=s['id']).values('id', 'name')

        for model in models:
            s['models'].append(model)

        data.append(s)

    return JsonResponse({
        'status': True,
        'series': data
    })
