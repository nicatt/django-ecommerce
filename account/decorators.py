from django.http import Http404
from functools import wraps


def logout_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            raise Http404
        else:
            return func(request, *args, **kwargs)

    return wrapper
