from django.db import models
from .enums import EngineTypes, LayoutTypes, Colors, BodyTypes, GearTypes


# Create your models here.
class Brand(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'brands'

    def __str__(self):
        return self.name


class ModelSeries(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'brand_model_series'

    def __str__(self):
        return self.name


class Model(models.Model):
    series = models.ForeignKey(ModelSeries, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'brand_models'

    def __str__(self):
        return self.name


class Auto(models.Model):
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    production_year = models.IntegerField()
    engine_type_id = models.IntegerField(choices=EngineTypes.choices())
    body_type_id = models.IntegerField(choices=BodyTypes.choices())
    gear_type_id = models.IntegerField(choices=GearTypes.choices())
    drive_type_id = models.IntegerField(choices=LayoutTypes.choices())
    color_id = models.IntegerField(choices=Colors.choices())
    mileage = models.IntegerField()
    engine_volume = models.IntegerField()
    horse_power = models.IntegerField()
    price = models.IntegerField()
    supplies = models.TextField()

    class Meta:
        db_table = 'autos'

    def __str__(self):
        return self.model.series.brand.name + ' ' + self.model.name
