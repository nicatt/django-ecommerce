from django import forms
from .models import Auto, Model
from .enums import Colors, EngineTypes, BodyTypes, GearTypes, LayoutTypes, AutoSupplies


class AutoAdminForm(forms.ModelForm):
    engine_type_id = forms.ChoiceField(label="Engine", widget=forms.Select, choices=EngineTypes.choices())
    color_id = forms.ChoiceField(label="Color", widget=forms.Select, choices=Colors.choices())
    body_type_id = forms.ChoiceField(label="Body Type", widget=forms.Select, choices=BodyTypes.choices())
    gear_type_id = forms.ChoiceField(label="Gear", widget=forms.Select, choices=GearTypes.choices())
    drive_type_id = forms.ChoiceField(label="Layout", widget=forms.RadioSelect, choices=LayoutTypes.choices())
    supplies = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=AutoSupplies.choices())

    class Meta:
        model = Auto
        fields = (
            'model',
            'production_year',
            'mileage',
            'engine_volume',
            'horse_power',
            'price',
            'supplies',
        )
        labels = {
            "engine_volume": "Engine volume (sm3)",
            "price": "Price (USD)",
        }


class AddAutoForm(forms.ModelForm):
    drive_type_id = forms.ChoiceField(label="Layout", widget=forms.RadioSelect, choices=LayoutTypes.choices())
    supplies = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=AutoSupplies.choices())

    class Meta:
        model = Auto
        fields = (
            'production_year',
            'engine_type_id',
            'color_id',
            'body_type_id',
            'gear_type_id',
            'drive_type_id',
            'mileage',
            'engine_volume',
            'horse_power',
            'price',
            'supplies',
        )
        labels = {
            "engine_volume": "Engine volume (sm3)",
            "price": "Price (USD)",
        }
