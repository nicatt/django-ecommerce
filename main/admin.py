from django.contrib import admin
from .models import Brand, ModelSeries, Model, Auto
from .forms import AutoAdminForm


# Register your models here.
class AutoAdmin(admin.ModelAdmin):
    fieldsets = [
        ('General', {'fields': [
            'model',
            'body_type_id',
            'production_year',
            'price',
            'mileage',
            'color_id',
        ]}),
        ('Specifications', {'fields': [
            'engine_type_id',
            'gear_type_id',
            'drive_type_id',
            'engine_volume',
            'horse_power',
        ]}),
        ('Extra Supplies', {
            'classes': ('collapse',),
            'fields': [
                'supplies'
            ]
        })
    ]
    form = AutoAdminForm


admin.site.register(Brand)
admin.site.register(ModelSeries)
admin.site.register(Model)
admin.site.register(Auto, AutoAdmin)
