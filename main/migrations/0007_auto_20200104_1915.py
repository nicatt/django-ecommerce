# Generated by Django 3.0.1 on 2020-01-04 15:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20191231_1649'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auto',
            name='engine_type_id',
            field=models.IntegerField(default=None),
        ),
        migrations.AlterField(
            model_name='auto',
            name='gear_type_id',
            field=models.IntegerField(),
        ),
    ]
