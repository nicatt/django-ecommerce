# Generated by Django 3.0.1 on 2019-12-31 09:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20191231_1352'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='modelseries',
            table='model_series',
        ),
    ]
