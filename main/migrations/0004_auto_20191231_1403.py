# Generated by Django 3.0.1 on 2019-12-31 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20191231_1355'),
    ]

    operations = [
        migrations.RenameField(
            model_name='auto',
            old_name='model_id',
            new_name='model',
        ),
        migrations.RenameField(
            model_name='model',
            old_name='series_id',
            new_name='serie',
        ),
        migrations.RenameField(
            model_name='modelseries',
            old_name='brand_id',
            new_name='brand',
        ),
    ]
