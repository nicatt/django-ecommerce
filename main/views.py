from django.shortcuts import render
from .models import Auto, Brand, Model
from .forms import AddAutoForm
from .utils import check_params


# Create your views here.
def home(request):
    return render(request, 'home.html', {'autos': Auto.objects.all()})


def add_auto(request):
    form = AddAutoForm(None)

    if request.method == 'POST':
        form = AddAutoForm(request.POST)
        if form.is_valid() and check_params(request.POST, ['model']):
            model_id = request.POST.get('model')
            try:
                auto = Auto(
                    model=Model.objects.get(pk=model_id),
                    production_year=request.POST.get('production_year'),
                    engine_type_id=request.POST.get('engine_type_id'),
                    body_type_id=request.POST.get('body_type_id'),
                    gear_type_id=request.POST.get('gear_type_id'),
                    drive_type_id=request.POST.get('drive_type_id'),
                    color_id=request.POST.get('color_id'),
                    mileage=request.POST.get('mileage'),
                    engine_volume=request.POST.get('engine_volume'),
                    horse_power=request.POST.get('horse_power'),
                    price=request.POST.get('price'),
                    supplies=request.POST.get('supplies')
                )
                auto.save()
            except Exception:
                pass

    return render(request, 'new_auto.html', {
        'brands': Brand.objects.all(),
        'form': form
    })
