def check_params(array, params):
    if len(array) == 0:
        return False

    state = True

    for param in params:
        if param not in array:
            state = False
            break

    return state
