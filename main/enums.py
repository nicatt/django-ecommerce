from enum import IntEnum


class AutoSupplies(IntEnum):
    # region ENUMS
    Power_windows = 1
    Leather_seats = 2
    Light_alloy_wheels = 3
    Conditioner = 4
    Climate_control = 5
    Sunroof = 6
    Parktronic = 7
    Navigation = 8
    Cruise_control = 9
    Rain_sensor = 10
    Light_sensor = 11
    Xenon_lamps = 12
    Back_camera = 13
    Side_curtains = 15
    Heated_seats = 16
    Cooled_seats = 17
    # endregion

    @classmethod
    def choices(cls):
        return [(key.value, key.name.replace("_", " ")) for key in cls]


class Colors(IntEnum):
    # region ENUMS
    Mixed = 1
    Black = 2
    Brown = 3
    Gray = 4
    Yellow = 5
    Silver = 6
    Golden = 7
    Beige = 8
    Blue = 9
    Green = 10
    Orange = 11
    White = 12
    Red = 13
    Navy = 15
    # endregion

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]


class EngineTypes(IntEnum):
    # region ENUMS
    Petrol = 1
    Diesel = 2
    Gas = 3
    Hybrid = 4
    Electric = 5
    Truck = 6
    Motorcycle = 7
    Hatchback = 8
    Minivan = 9
    Cabriolet = 10
    Pickup = 11
    Bus = 12
    # endregion

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]


class BodyTypes(IntEnum):
    # region ENUMS
    Sedan = 1
    SUV = 2
    Wagon = 3
    Sport_car = 4
    Minibus = 5
    # endregion

    @classmethod
    def choices(cls):
        return [(key.value, key.name.replace("_", " ")) for key in cls]


class GearTypes(IntEnum):
    # region ENUMS
    Manual = 1
    Automatic = 2
    Robot = 3
    Variator = 4
    No_gear = 5
    # endregion

    @classmethod
    def choices(cls):
        return [(key.value, key.name.replace("_", " ")) for key in cls]


class LayoutTypes(IntEnum):
    # region ENUMS
    Front_wheel = 1
    Rear_wheel = 2
    Four_wheel = 3
    # endregion

    @classmethod
    def choices(cls):
        return [(key.value, key.name.replace("_", " ")) for key in cls]